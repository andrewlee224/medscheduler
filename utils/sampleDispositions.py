'''
Script for creating sample JSON files
containing drug disposition info
'''

import json

def createPatient(firstname, lastname, pesel, roomNum):
    patientDict = { 
        'firstname': firstname,
        'lastname': lastname,
        'PESEL': pesel,
        'roomNum': roomNum,
        'dispositions': None
        }

    return patientDict

def createSingleDisposition(
    timeHour = 8,
    drugType = "IV",
    drugName = "Morphine",
    dosage = 2):

    singleDisp = { 
        'timeHour': timeHour,
        'drugType': drugType,
        'drugName': drugName,
        'dosage': dosage,
        }

    return singleDisp

if __name__ == '__main__':
    patient1 = createPatient("Andrew", "Lichaczewski", "90043099999", 3)
    patient2 = createPatient("Liam", "Neeson", "50040299999", 2)
    patient3 = createPatient("John", "Nawrot", "91040404040", 2)
    disp1 = createSingleDisposition()
    disp2 = createSingleDisposition(dosage=1)
    disp3 = createSingleDisposition(
        drugType='pills',
        drugName='LSD',
        dosage=3)
    disp4 = createSingleDisposition(
        drugType='pills',
        drugName='Pluszzz',
        timeHour=13)

    patient1['dispositions'] = [ disp1 ]
    patient2['dispositions'] = [ disp2, disp3 ]
    patient3['dispositions'] = [ disp4 ]

    groupedDisp = [ patient1, patient2, patient3 ]
    dispJSON = json.dumps(groupedDisp, sort_keys=True, indent=4)
    print(dispJSON)

    f = open("disposition.json", "wb")
    f.write(dispJSON)
    f.close()
