package com.example.medscheduler;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.medscheduler.data.DrugDisposition;

public class DispositionAdapter extends ArrayAdapter<DrugDisposition> {
	private Context context;
	private DispositionAdapter adapter = this;
	private List<DrugDisposition> dispositions;
	
	public DispositionAdapter(Context context, List<DrugDisposition> dispositions) {
		super(context, R.layout.disposition_row, dispositions);
		this.context = context;
		this.dispositions = dispositions;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.disposition_row, parent, false);
		
		TextView drugType = (TextView) rowView.findViewById(R.id.drugType);
		TextView drugName = (TextView) rowView.findViewById(R.id.drugName);
		TextView drugHour = (TextView) rowView.findViewById(R.id.drugHour);
		TextView drugDose = (TextView) rowView.findViewById(R.id.drugDose);
		Button confirmButton = (Button) rowView.findViewById(R.id.confirmButton);
		
		DrugDisposition disposition = dispositions.get(position);
		
		drugType.setText(disposition.getDrugType().toString());
		drugName.setText(disposition.getDrugName());
		drugHour.setText(disposition.getFormattedHour());
		drugDose.setText(disposition.getFormattedDosage());
		
		final int listenerPosition = position;
		OnLongClickListener confirmClickListener = new OnLongClickListener() {
			public boolean onLongClick(View v) {
				dispositions.remove(listenerPosition);
				adapter.notifyDataSetChanged();
				if(dispositions.isEmpty()) {
					((DispositionActivity) context).backAndRemove();
				}
				
				return true;
			}
		};
		
		confirmButton.setOnLongClickListener(confirmClickListener);
		
		return rowView;
	}

}
