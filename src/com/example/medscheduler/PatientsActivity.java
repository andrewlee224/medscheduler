package com.example.medscheduler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.medscheduler.data.DownloadTask;

public class PatientsActivity extends Activity {

	private Context context = this;
	private ListView patientsListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patients);
        
        patientsListView = (ListView) findViewById(R.id.patientsListView);
        RelativeLayout progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        patientsListView.setOnItemClickListener(patientListListener);
        
        DownloadTask dt = new DownloadTask(this, patientsListView, progressLayout);
        dt.execute();
        patientsListView.invalidate();
        
	}

	OnItemClickListener patientListListener = 
			new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(context, DispositionActivity.class);
			intent.putExtra("patientId", position);
			startActivity(intent);
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patients, menu);
		return true;
	}

}
