package com.example.medscheduler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	Context context = this;
	EditText passwordEdit;
	EditText usernameEdit;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(loginListener);
        passwordEdit = (EditText) findViewById(R.id.passwordEdit);
        usernameEdit = (EditText) findViewById(R.id.usernameEdit);
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    OnClickListener loginListener = new OnClickListener() {
    	public void onClick(View v) {
    		if (usernameEdit.getText().toString().equals("nurse") 
    				&& passwordEdit.getText().toString().equals("apple")) {
	    		Intent intent = new Intent(context, PatientsActivity.class);
	    		startActivity(intent);
    		} else {
    			Toast.makeText(context, "Wrong username/password", Toast.LENGTH_SHORT).show();
    		}
    	}
    };
    
}
