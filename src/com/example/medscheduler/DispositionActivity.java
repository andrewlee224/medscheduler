package com.example.medscheduler;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;

import com.example.medscheduler.data.DataHolder;
import com.example.medscheduler.data.DrugDisposition;
import com.example.medscheduler.data.Patient;

public class DispositionActivity extends Activity {

	Patient patient;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_disposition);
		
		Bundle bundle = getIntent().getExtras();
		int patientId = bundle.getInt("patientId");
		patient = DataHolder.getPatient(patientId);
		TextView patientName = (TextView) findViewById(R.id.patientName);
		TextView patientPesel = (TextView) findViewById(R.id.patientPesel);
		TextView patientRoom = (TextView) findViewById(R.id.patientRoom);
		patientName.setText(patient.getLastName() + ", " + patient.getFirstName());
		patientPesel.setText(patient.getPesel());
		patientRoom.setText(Integer.toString(patient.getRoomNum()));
		
		List<DrugDisposition> dispositions = patient.getDispositions();
		ListView dispositionList = (ListView) findViewById(R.id.dispositionList);
		DispositionAdapter dispositionAdapter = new DispositionAdapter(this, dispositions);
		dispositionList.setAdapter(dispositionAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.disposition, menu);
		return true;
	}
	
	public void backAndRemove() {
		DataHolder.removePatient(patient);
		finish();
	}

}
