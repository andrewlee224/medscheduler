package com.example.medscheduler.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.SparseArray;

public class JSONParser {
	List<Patient> patients = new ArrayList<Patient>();
	
	public List<Patient> extractDispositions(JSONArray jsonArr) throws JSONException {
		
		//Iterate through every patient
		for(int i = 0; i < jsonArr.length(); i++) {
			Patient patient = new Patient();
			JSONObject patientObj = (JSONObject) jsonArr.get(i);
			patient.setFirstName(patientObj.getString("firstname"));
			patient.setLastName(patientObj.getString("lastname"));
			patient.setPesel(patientObj.getString("PESEL"));
			patient.setRoomNum(patientObj.getInt("roomNum"));
			
			//Get dispositions from JSON and add them to Patient object
			JSONArray dispositionArr = (JSONArray) patientObj.getJSONArray("dispositions");
			//iterate through dispositions
			for(int j = 0; j < dispositionArr.length(); j++) {
				JSONObject dispositionObj = (JSONObject) dispositionArr.get(j);
				DrugDisposition disposition = new DrugDisposition();
				disposition.setDrugTypeStr(dispositionObj.getString("drugType"));
				disposition.setDrugName(dispositionObj.getString("drugName"));
				disposition.setDosage(dispositionObj.getInt("dosage"));
				disposition.setTimeHour(dispositionObj.getInt("timeHour"));
				
				patient.addDisposition(disposition);
			}
			patients.add(patient);
		}
		
		return patients;
	}
	
	public List<Patient> extractDispositionsAlt(JSONArray patientArr, JSONArray drugArr)  
		throws JSONException {
		
		SparseArray<Patient> patientMap = new SparseArray<Patient>();
		
		for(int i = 0; i < patientArr.length(); i++) {
			Patient patient = new Patient();
			JSONObject patientObj = (JSONObject) patientArr.get(i);
			patient.setFirstName(patientObj.getString("name"));
			patient.setLastName(patientObj.getString("surname"));
			patient.setPesel(patientObj.getString("pesel"));
			patient.setRoomNum(patientObj.getInt("room"));
			patient.setDbId(patientObj.getInt("id"));
			
			patients.add(patient);
			patientMap.put(patient.getDbId(), patient);
		}
		
		for(int i = 0; i < drugArr.length(); i++) {
			DrugDisposition disposition = new DrugDisposition();
			JSONObject dispositionObj = (JSONObject) drugArr.get(i);
			disposition.setDrugTypeStr(dispositionObj.getString("category"));
			if (disposition.getDrugTypeStr().equals("Kroplowka")) {
				disposition.setDrugType(DrugType.IV);
			} else if (disposition.getDrugTypeStr().equals("Tabletki")) {
				disposition.setDrugType(DrugType.Pills);
			} else if (disposition.getDrugTypeStr().equals("Syrop")) {
				disposition.setDrugType(DrugType.Syrup);
			}
			disposition.setDrugName(dispositionObj.getString("name"));
			disposition.setDosage(dispositionObj.getInt("dose"));
			disposition.setTimeHour(dispositionObj.getInt("hour"));
			disposition.setDbId(dispositionObj.getInt("id"));
			
			int boundPatientDbId = dispositionObj.getInt("patient_id");
			Patient boundPatient = patientMap.get(boundPatientDbId);
			boundPatient.addDisposition(disposition);
		}
		
		cleanPatients(patients);
		for(int i = 0; i < patients.size(); i++) {
			Patient patient = patients.get(i);
			Collections.sort(patient.getDispositions(), new DispositionComparator());
		}
		Collections.sort(patients, new PatientComparator());
		return patients;
	}

	public void cleanPatients(List<Patient> patients) {
		List<Patient> patientsToRemove = new ArrayList<Patient>();
		for(int i = 0; i < patients.size(); i++) {
			Patient patient = patients.get(i);
			if (patient.getNumDispositions() < 1) {
				patientsToRemove.add(patient);
			}
		}
		
		for(int i = 0; i < patientsToRemove.size(); i++) {
			patients.remove(patientsToRemove.get(i));
		}
	}
	
}

