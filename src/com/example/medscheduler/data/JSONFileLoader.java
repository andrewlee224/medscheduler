package com.example.medscheduler.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.example.medscheduler.R;

/**
 * Open a JSON file and load data into a JSONObject from a resource
 * @author Andrew
 *
 */
public class JSONFileLoader {
	private InputStream is;
	
	public String loadJSONString(Context context) throws IOException {
		return loadJSONString(context, R.raw.disposition);
	}
	
	public String loadJSONString(Context context, int resId) throws IOException {
		is = context.getResources().openRawResource(resId);
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		
		try {
			Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		}
		finally {
			is.close();
		}
		
		return writer.toString();
	}
	
	public JSONObject loadJSONObject(Context context, int resId) throws IOException, 
		JSONException {
		return new JSONObject(loadJSONString(context, resId));
	}
	
	public JSONObject loadJSONObject(Context context) throws IOException, JSONException {
		return new JSONObject(loadJSONString(context));
	}
	
	public JSONArray loadJSONArray(Context context, int resId) throws IOException, 
		JSONException {
		return new JSONArray(loadJSONString(context, resId));
	}
	
	public JSONArray loadJSONArray(Context context) throws IOException, JSONException {
		return new JSONArray(loadJSONString(context));
	}
	
	public JSONArray downloadJSON(String filename) throws ClientProtocolException, 
		IOException, JSONException {
		HttpClient httpClient = new DefaultHttpClient();
		String url = "http://boiling-headland-9900.herokuapp.com/" + filename;
		HttpGet httpGet = new HttpGet(url);
		
		HttpResponse httpResponse = httpClient.execute(httpGet);
		String stringJSON = "";
		if (httpResponse != null) {
			InputStream is = httpResponse.getEntity().getContent();
			stringJSON = convertStreamToString(is);
		} else {
			throw new IOException("Null httpResponse");
		}
		
		return new JSONArray(stringJSON);
	}
	
	public String convertStreamToString(InputStream is) throws IOException {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
	
		while ((line = br.readLine()) != null) {
			total.append(line);
		}
		
		return total.toString();
	}
	
}

