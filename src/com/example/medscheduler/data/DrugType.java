package com.example.medscheduler.data;

public enum DrugType {
	IV, Syrup, Pills;
}
