package com.example.medscheduler.data;

import java.util.ArrayList;
import java.util.List;

/*
 * Class containing data for a single patient
 */

public class Patient {
	
	private static int counter = 0;
	
	private int id;	//id for application internal purposes
	private int dbId;	//id from database (primary key)
	private String firstName;
	private String lastName;
	private String pesel;
	private int roomNum;
	
	private List<DrugDisposition> dispositions = new ArrayList<DrugDisposition>();
	
	public Patient() {
		this.id = counter;
		Patient.counter += 1;
	}
	public static int getCounter() {
		return Patient.counter;
	}
	public int getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public int getRoomNum() {
		return roomNum;
	}
	public void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}
	public void addDisposition(DrugDisposition disp) {
		dispositions.add(disp);
	}
	public List<DrugDisposition> getDispositions() {
		return dispositions;
	}
	public int getDbId() {
		return dbId;
	}
	public void setDbId(int dbId) {
		this.dbId = dbId;
	}
	public int getNumDispositions() {
		return dispositions.size();
	}
	@Override
	public String toString() {
		return (this.firstName.charAt(0) + ". " + this.lastName);
	}
}
