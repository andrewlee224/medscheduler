package com.example.medscheduler.data;

import java.util.Comparator;

public class DispositionComparator implements Comparator<DrugDisposition> {
	public int compare(DrugDisposition d1, DrugDisposition d2) {
		return d1.getTimeHour() - d2.getTimeHour();
	}
}
