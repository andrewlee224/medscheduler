package com.example.medscheduler.data;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import android.widget.Toast;

import com.example.medscheduler.DispositionActivity;
import com.example.medscheduler.PatientAdapter;

public class DownloadTask extends AsyncTask<Void, Void, Void> {

	private Context context;
	private ListView listView;
	private View progressLayout;
	
	private JSONArray patientsJSON;
	private JSONArray drugsJSON;
	private List<Patient> patients;
	
	private Exception exception;
	
	public DownloadTask(Context context, ListView listView, View progressLayout) {
		this.context = context;
		this.listView = listView;
		this.progressLayout = progressLayout;
	}
	
	protected Void doInBackground(Void... v) {
		JSONFileLoader jsonLoader = new JSONFileLoader();
		JSONParser jsonParser = new JSONParser();
		
		try {
			patientsJSON = jsonLoader.downloadJSON("patient.json");
			Log.i("doInBackground", "patientsJSON downloaded");
			drugsJSON = jsonLoader.downloadJSON("medicine.json");
			Log.i("doInBackground", "drugsJSON downloaded");
			patients = jsonParser.extractDispositionsAlt(patientsJSON, drugsJSON);
			Log.i("doInBackground", "Number of patients: " + 
					Integer.toString(patients.size()));
		} 
		catch (ClientProtocolException e) {
			exception = e;
			Log.i("ClientProtocolException", e.getMessage());
		}
		catch (IOException e) {
			exception = e;
			Log.i("IOException", e.getMessage());
		}
		catch (JSONException e) {
			exception = e;
			Log.i("JSONException", e.getMessage());
		}
		Log.i("DownloadTask", "Downloaded JSON and created patients");
		Log.i("First patient", patients.get(0).getFirstName());
		return null;
	}
	
	@Override
	protected void onPostExecute(Void v) {
		
		if (exception != null) {
			Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
		}
		
		PatientAdapter patientAdapter = new PatientAdapter(context, patients);
		listView.setAdapter(patientAdapter);
		Log.i("onPostExecute", "notifyDataSetChanged");
		DataHolder.setPatients(patients);
		
		progressLayout.setVisibility(View.GONE);
		listView.setVisibility(View.VISIBLE);
	}
	
	OnItemClickListener patientListListener = 
			new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(context, DispositionActivity.class);
			intent.putExtra("patientId", position);
			context.startActivity(intent);
		}
	};
}
