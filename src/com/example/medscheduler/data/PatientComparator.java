package com.example.medscheduler.data;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {
	public int compare(Patient p1, Patient p2) {
		return p1.getDispositions().get(0).getTimeHour() 
				- p2.getDispositions().get(0).getTimeHour();
	}
}
