package com.example.medscheduler.data;

import java.util.List;

public class DataHolder {
	private static List<Patient> patients;
	
	public static List<Patient> getPatients() {
		return DataHolder.patients;
	}
	public static Patient getPatient(int id) {
		return DataHolder.patients.get(id);
	}
	public static void removePatient(Patient patient) {
		patients.remove(patient);
	}
	public static void setPatients(List<Patient> patients) {
		DataHolder.patients = patients;
	}
}
