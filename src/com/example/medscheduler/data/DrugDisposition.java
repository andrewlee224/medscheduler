package com.example.medscheduler.data;

public class DrugDisposition {
	
	private static int counter = 0;
	
	private int id; //id for application internal purposes
	private int dbId; //id from database (primary key)
	private int timeHour;
	private String drugTypeStr;
	private DrugType drugType;
	private String drugName;
	private int dosage;
	
	public DrugDisposition() {
		DrugDisposition.counter += 1;
		this.id = counter;
	}
	public static int getCounter() {
		return DrugDisposition.counter;
	}
	public int getId() {
		return id;
	}
	public int getTimeHour() {
		return timeHour;
	}
	public void setTimeHour(int timeHour) {
		this.timeHour = timeHour;
	}
	public String getFormattedHour() {
		return (Integer.toString(timeHour) + ":00");
	}
	public String getDrugTypeStr() {
		return drugTypeStr;
	}
	public void setDrugTypeStr(String drugTypeStr) {
		this.drugTypeStr = drugTypeStr;
	}
	public DrugType getDrugType() {
		return drugType;
	}
	public void setDrugType(DrugType drugType) {
		this.drugType = drugType;
	}
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public int getDosage() {
		return dosage;
	}
	public String getFormattedDosage() {
		String units = "";
		if (drugType == DrugType.IV || drugType == DrugType.Syrup) {
			units = " ml";
		}
		return (Integer.toString(dosage) + units);
	}
	public void setDosage(int dosage) {
		this.dosage = dosage;
	}
	public int getDbId() {
		return dbId;
	}
	public void setDbId(int dbId) {
		this.dbId = dbId;
	}
}
