package com.example.medscheduler;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.medscheduler.data.DrugDisposition;
import com.example.medscheduler.data.Patient;

public class PatientAdapter extends ArrayAdapter<Patient> {
	private Context context;
	private List<Patient> patients;
	
	public PatientAdapter(Context context, List<Patient> patients) {
		super(context, R.layout.patient_row, patients);
		this.context = context;
		this.patients = patients;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.patient_row, parent, false);
		Patient patient = patients.get(position);
		TextView patientName = (TextView) rowView.findViewById(R.id.patientName);
		TextView roomNumber = (TextView) rowView.findViewById(R.id.roomNumber);
		TextView drugTime = (TextView) rowView.findViewById(R.id.drugTime);
		
		patientName.setText(patient.getLastName());
		roomNumber.setText(Integer.toString(patient.getRoomNum()));
		DrugDisposition firstDisp = patient.getDispositions().get(0);
		drugTime.setText(firstDisp.getFormattedHour());
		
		return rowView;
	}
}
